use anyhow::Result;
use std::path::PathBuf;

pub(crate) fn discord_dir() -> Result<PathBuf> {
    Ok("/Applications/Discord Canary.app/Contents/Resources/app".into())
}
