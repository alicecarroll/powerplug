# Powerplug
An unofficial [Powercord][powercord] installer and helper

# Installation

## From source

### Latest release

```sh
cargo install powerplug 
```
 
### From git

```sh
cargo install --git https://gitlab.com/johnmeow/powerplug.git
```

 ## From binaries

 ### Release

 Get the binaries from the [releases](/releases/) page

 ### Git

 Get the binaries from the latest [pipeline](../-/jobs/artifacts/master/browse?job=cargo:build)

 # TODO

- GUI
  (Currently `powerplug` binary panics on invocation)
- Plugin/theme manager
  - Installer/uninstaller
  - Updater


[powercord]: https://powercord.dev/
